.. index::
   pair: MongoDB ; Official Django MongoDB Backend Now Available in Public Preview

.. _django_mongodb_2025_02_03:

===================================================================================
2025-02-03 **Official Django MongoDB Backend Now Available in Public Preview**
===================================================================================

- https://www.mongodb.com/blog/post/mongodb-django-backend-now-available-public-preview
- https://www.mongodb.com/resources/basics/databases/document-databases
- https://github.com/mongodb-labs/django-mongodb-backend
- https://www.mongodb.com/community/forums/t/accidental-release-of-pymongo-and-django-mongodb-backend-10-10-10-10/311058


We are pleased to announce that the Official Django MongoDB Backend Public 
Preview is now available. 

This Python package makes it easier than ever to combine the sensible defaults 
and fast development speed Django provides with the convenience and ease 
of MongoDB.

Building for the Python community
======================================

**For years, Django has been consistently rated one of the most popular web 
frameworks in the Python ecosystem.** 

It’s a powerful tool for building web applications **quickly and securely**, and 
implements best practices by default while abstracting away complexity. 

Over the last few years, Django developers have increasingly used MongoDB, 
presenting an opportunity for an official MongoDB-built Python package to make 
integrating both technologies as painless as possible.

We recognize that success in this endeavor requires more than just technical 
expertise in database systems—it demands a deep understanding of Django's ecosystem, 
conventions, and the needs of its developer community. 

So we’re committed to ensuring that the Official Django MongoDB Backend not 
only meets the technical requirements of developers, but **also feels painless 
and intuitive, and is a natural complement to the base Django framework**.

