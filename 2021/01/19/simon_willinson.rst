.. index::
   ! Datasette


.. _simonw_2021_01_20:

===========================================================================
Personal search engine with **datasette and dogsheep** by Simon Willison
===========================================================================

.. seealso::

   - https://x.com/simonw
   - https://talkpython.fm/episodes/show/299/personal-search-engine-with-datasette-and-dogsheep
   - https://x.com/simonw/status/1351317682642059266?s=20



Twitter announce
===================

My @talkpython episode with @mkennedy is out!

We cover the early history of Django, how Datasette came about, the
evolution of my Dogsheep collection of tools and a whole lot more

I'm really happy with this - I think it's my best podcast recording yet


In this episode, we'll be discussing two powerful tools for data
reporting and exploration: Datasette and Dogsheep.

Datasette helps people take data of any shape or size, analyze and
explore it, and publish it as an interactive website and accompanying API.

Dogsheep is a collection of tools for personal analytics using SQLite
and Datasette. Imagine a unified search engine for everything personal
in your life such as twitter, photos, google docs, todoist, goodreads,
and more, all in once place and outside of cloud companies.

On this episode we talk with Simon Willison who created both of these
projects.

He's also one of the co-creators of Django and we'll discuss some early
Django history!

Transcript
=============

.. seealso::

   - https://talkpython.fm/episodes/transcript/299/personal-search-engine-with-datasette-and-dogsheep

Links from the show
=====================

- `Datasette io <https://datasette.io/>`_
- `Dogsheep <https://dogsheep.github.io/>`_
- `Datasheet newsletter: <https://datasette.substack.com/>`_
- `Video: Build your own data warehouse for personal analytics with <youtube.com <https://www.youtube.com/watch?v=CPQCD3Qxxik>`_

Examples
==========

- `List datasette github.com <https://github.com/simonw/datasette/wiki/Datasettes>`_
- `Personal data warehouses: <github.com <https://simonwillison.net/2020/Nov/14/personal-data-warehouses/>`_
- `Global power plants  <https://global-power-plants.datasettes.com/>`_
- `SF data <https://san-francisco.datasettes.com/>`_
- `FiveThirtyEight <fivethirtyeight.datasettes.com>`_
- `Lahman’s Baseball Database: <https://baseballdb.lawlesst.net/>`_
- `Live demo of current main: <datasette.io <https://latest.datasette.io/>`_
