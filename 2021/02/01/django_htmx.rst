.. index::
   pair: Django; htmx


.. _django_htmx_2021_02_01:

===========================================================
**django-htmx** (Extensions for using Django with **htmx**)
===========================================================

- :ref:`django_htmx`
