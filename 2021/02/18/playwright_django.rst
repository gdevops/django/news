.. index::
   pair: Django ; Playwright


.. _playright_django_e2e:

=====================================================================
**Playwright with Django  Reliable end-to-end testing for the web**
=====================================================================

.. seealso::

   - https://devblogs.microsoft.com/python/announcing-playwright-for-python-reliable-end-to-end-testing-for-the-web/
   - https://aandryashin.medium.com/playwright-launching-cross-browser-automation-to-the-stars-4a9cca8f0df0
   - https://x.com/arjunattam
   - https://github.com/marketplace/actions/run-playwright-tests
   - :ref:`playwright_with_django_2021_02_18`




Playwright with Django
=========================


You can use Playwright to test views in Django web apps.

To install Playwright, and the browsers to test on, run::

    pip install playwright
    python –m playwright install

**Playwright integrates with the built-in testing tools in Django.**

Specifically, you can use the LiveServerTestCase class to launch a live
Django server and run browser tests against it.

.. code-block:: python
   :linenos:

    from django.contrib.staticfiles.testing import StaticLiveServerTestCase
    from playwright import sync_playwright

    class MyViewTests(StaticLiveServerTestCase):
        @classmethod
        def setUpClass(cls):
            super().setUpClass()
            cls.playwright = sync_playwright().start()
            cls.browser = cls.playwright.chromium.launch()

        @classmethod
        def tearDownClass(cls):
            cls.browser.close()
            cls.playwright.stop()
            super().tearDownClass()

        def test_login(self):
            page = self.browser.newPage()
            page.goto('%s%s' % (self.live_server_url, '/login/'))
            page.fill('#username', 'myuser')
            page.fill('#password', 'secret')
            page.click('text=Log in')
            assert page.url == '%s%s' % (self.live_server_url, '/profile/')
            page.close()



Another example
------------------

.. seealso::

   - :ref:`playwright_with_django_2021_02_18`


.. literalinclude:: pw_recherche.py
   :linenos:


Deploy Playwright tests to CI/CD
===================================

.. seealso::

   - https://github.com/marketplace/actions/run-playwright-tests
   - https://playwright.dev/#path=docs%2Fci.md&q=
   - https://github.com/microsoft/playwright-python
   - https://github.com/microsoft/playwright-python/issues

Running end-to-end tests in your CI/CD pipelines helps catch issues
early.

You can deploy Playwright tests to CI/CD with the `Playwright GitHub Action <https://github.com/marketplace/actions/run-playwright-tests>`_
or with tools for other `CI/CD providers <https://playwright.dev/#path=docs%2Fci.md&q=>`_.

`Playwright for Python <https://github.com/microsoft/playwright-python>`_
is built in the open on GitHub, and we are eager to learn more on how
Playwright works for you.

Feel free to share feedback or feature requests on GitHub issues or join
the Playwright Slack community to connect with other users.
