

.. _django_2021_07_02:

==============================================
2021-07-02 https://django-news.com/issues/82
==============================================

- https://django-news.com/issues/82


db/optimization
=================

- https://docs.djangoproject.com/en/3.2/topics/db/optimization/



How to Start a Production-Ready Django Project
================================================

- https://simpleisbetterthancomplex.com/tutorial/2021/06/27/how-to-start-a-production-ready-django-project.html

In this tutorial I’m going to show you how I usually start and organize
a new Django project nowadays.

I’ve tried many different configurations and ways to organize the project,
but for the past 4 years or so this has been consistently my go-to setup.

Please note that this is not intended to be a “best practice” guide or
to fit every use case. It’s just the way I like to use Django and that’s
also the way that I found that allow your project to grow in healthy way.


YAGNI exceptions
==================

- https://lukeplant.me.uk/blog/posts/yagni-exceptions/

I’m essentially a believer in You Aren’t Gonna Need It — the principle
that you should add features to your software — including generality
and abstraction — when it becomes clear that you need them, and not before.

However, there are some things which really are easier to do earlier
than later, and where natural tendencies or a ruthless application of
YAGNI might neglect them. This is my collection so far:
