

======================================================================
2021-05-20 HTMX & Django—bringing the new school to the old school
======================================================================

- https://x.com/_craiga
- https://www.sharperinfo.com/post/htmx-django-bringing-the-new-school-to-the-old-school
- https://blog-demo-django-app.sharperinfo.com/order-meal/

Introduction
===============

HTMX is a new JavaScript framework.

If you're anything like me, that's normally enough to make you roll
your eyes, close this browser tab and get on with your day.

But wait! HTMX is different! It's a framework which embraces the old school.

It plays well with any server-side framework, and embraces the RESTful
architecture of the web.

Read on for a bit of background, or jump ahead to see how it can be
integrated into your existing Django app.


A practical example
=====================

At My Data Chameleon, we've spent months building complex data checking
rules to guide users through optimising their data for the CPUC CET.

But users were only seeing the results of that work after filling in
tens of fields and clicking a button.

With HTMX and an afternoon of work, we were able to give users dynamic
feedback as they fixed their data.

The world of energy efficiency isn't easy to understand, so I've come up
with an example form to demonstrate how we've used HTMX to give users
dynamic feedback on the data they've entered.


Conclusion
=============

And that is that! This form now updates itself whenever the user changes
any field, giving  immediate feedback on the data they've entered without
having to re-implement any of our validation client-side.

Feel free to see this in action in our demo app. If you want to see this
demo app's code, it's available on GitHub.

If you'd like to share any thought about this article, please reach out!
You can reach Sharper Informatics Solutions on @SharperInfo, or get in
touch with the author directly at @_craiga.
