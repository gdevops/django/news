
.. _django_readers_2021_12_17:

=======================================================================================================================================================================================================
2021-12-17 **django-readers (A lightweight function-oriented toolkit for better organisation of business logic and efficient selection and projection of data in Django projects)** by Jamie Matthews
=======================================================================================================================================================================================================

- :ref:`django_readers`
- :ref:`jamie_matthews`
