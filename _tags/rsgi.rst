.. _sphx_tag_rsgi:

My tags: RSGI
#############

.. toctree::
    :maxdepth: 1
    :caption: With this tag

    ../2024/04/03/asgi-deployment-options-for-django.rst
