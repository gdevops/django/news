
.. index::
   pair: Django ; ASGI
   pair: Django ; 3.0

.. _django_asgi_2019_04_13:

==========================================================
Issue 11209: Implement ASGI handler and coroutine-safety
==========================================================

.. seealso::

   - https://github.com/django/django/pull/11209
   - https://x.com/andrewgodwin/status/1117106120282255361
   - https://github.com/django/asgiref/blob/master/asgiref/local.py



Twitter Announce
==================

.. figure:: announce_asgi_pull_request.png
   :align: center


So, I have a draft pull request against Django that makes it run under ASGI as
well as WSGI.

Not ready to land yet, but it's not looking bad at all !


...

Indeed, and also because I was not prepared to push an idea into core Django
before it had time to bake.

Channels was a deliberate proving ground (three different designs over three
years).


Issue 11209
=============

This adds in an AsgiHandler handler class, get_asgi_application, and an
asgi.py file into the default project template.

It serves Django over an ASGI interface, but does not yet allow asynchronous
views; get_response is where the context changes from asynchronous to synchronous.

It also swaps out Django's threading-based safety for asgiref.local, which
provides hybrid thread- and async-task-based safety.

The goal would be to land this in master, and then start work on getting async
through the middleware layers and down to views if they are declared as
async def.

That way we can stabilise this, make sure it's OK, and hopefully ship it in 3.0,
and then see how far we can get async view handling as another PR so it's not
all tied up in one big patch.
