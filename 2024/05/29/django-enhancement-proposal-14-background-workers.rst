.. index::
   pair: DEP14 ; Django Enhancement Proposal 14: Background Workers Approved (2024-05-29)


.. _django_dep_14_2024_06_03:

=====================================================================================
2024-05-29 DEP14 **Django Enhancement Proposal 14: Background Workers** Approved 🛫
=====================================================================================

- https://www.djangoproject.com/weblog/2024/may/29/django-enhancement-proposal-14-background-workers/
- https://github.com/django/deps/blob/main/accepted/0014-background-workers.rst
- https://github.com/django/deps/pull/86 ( Background workers #86 )
- https://github.com/RealOrangeOne
- https://mastodon.theorangeone.net/@jake
- https://mastodon.theorangeone.net/@jake/112525872906468762
- https://fosstodon.org/@django/112526272305834686

As of today, DEP-14 has been approved 🛫

The DEP was written and stewarded by `Jake Howard <https://github.com/RealOrangeOne>`_.

A very enthusiastic community has been active with feedback and encouragement,
while the Django Steering Council gave the final inputs before its formal acceptance.

The implementation of DEP-14 is expected to be a major leap forward for the
“batteries included” philosophy of Django.


Whilst Django is a web framework, there's more to web applications than just
the request-response lifecycle.

Sending emails, communicating with external services or running complex actions
should all be done outside the request-response cycle.

Django doesn't have a first-party solution for long-running tasks, however
the ecosystem is filled with incredibly popular frameworks, all of which interact
with Django in slightly different ways.

Other frameworks such as Laravel have background workers built-in, allowing
them to push tasks into the background to be processed at a later date,
without requiring the end user to wait for them to occur.

Library maintainers must implement support for any possible task backend
separately, should they wish to offload functionality to the background.

This includes smaller libraries, but also larger meta-frameworks with their
own package ecosystem such as Wagtail.

This proposal sets out to provide an interface and base implementation for
long-running background tasks in Django.


Future work
=============

- https://github.com/RealOrangeOne/django-tasks
- https://2024.djangocon.eu/talks/schedule/
- https://pretalx.evolutio.pt/djangocon-europe-2024/talk/VDYCVB/

The DEP will now move on to the Implementation phase before being merged
into Django itself.

If you would like to help or try it out, go have a look at `django-tasks <https://github.com/RealOrangeOne/django-tasks>`_, a
separate reference implementation by Jake Howard, the author of the DEP.

Jake will also be speaking about the DEP `in his talk at DjangoCon Europe <https://pretalx.evolutio.pt/djangocon-europe-2024/talk/VDYCVB/>`_ at
`DjangoCon Europe 2024 <https://2024.djangocon.eu/>`_ in Vigo next week.

