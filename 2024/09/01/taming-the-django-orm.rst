

.. _hang_2024_09_01:

============================================================================================
2024-09-01 Taming the beast that is the Django ORM - An introduction by David Hang
============================================================================================

- https://www.davidhang.com/blog/2024-09-01-taming-the-django-orm/


The material this blog post was originally developed from was a bunch of 
slides used for a skill share presentation I gave at my workplace @ coreplan.io.

I have 3+ years of experience with Django, with it being the main framework 
that underpins the backend of CorePlan’s main SaaS product. 

It is a mature, batteries included framework that has been around for a 
while now. 

**One particular powerful yet dangerous feature of Django is the ORM**. 

**This is a Django specific ORM which cannot be separated from the rest of 
the framework**. 

The other major python ORM is SQLAlchemy which can be used with other 
python web frameworks, but is an independent tool.

Below are some of the things that I have learned about the Django ORM, 
how it compares to raw SQL and gotchas that you should be aware of when 
using it.
