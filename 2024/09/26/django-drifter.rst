.. index::
   pair: Django ; migrations
   ! django-drifter

.. _django_drifter:

=========================================================================================================================================
**django-drifter** (A small Django utility to make it easier to revert and redo migrations or to recreate your database. )
=========================================================================================================================================

- https://github.com/kennethlove/django-drifter


A small Django utility to make it easier to revert and redo migrations 
or to recreate your database. 
