.. index::
   pair: DJP ; A plugin system for Django

.. _djp_2024_09_25:

=====================================================
2024-09-25 **DJP: A plugin system for Django**
=====================================================

- https://simonwillison.net/2024/Sep/25/djp-a-plugin-system-for-django/
- https://simonw.substack.com/p/llama-32-and-plugins-for-django
- https://fedi.simonwillison.net/@simon/113201098719585146
- https://fedi.simonwillison.net/@simon/113198559373965992
- https://djp.readthedocs.io/en/latest/
- https://pluggy.readthedocs.io/en/stable/

DJP: A plugin system for Django
====================================

`DJP <https://djp.readthedocs.io/en/latest/>`_ is a new plugin mechanism for Django, built on top of `Pluggy <https://pluggy.readthedocs.io/en/stable/>`_. 

I announced the first version of DJP during my talk yesterday at DjangoCon US 2024, 
How to design and implement extensible software with plugins. 

I’ll post a full write-up of that talk once the video becomes available—this 
post describes DJP and how to use what I’ve built so far.


Why plugins? 
=================

Django already has a thriving ecosystem of third-party apps and extensions. 

What can a plugin system add here ?

If you’ve ever installed a Django extension—such as django-debug-toolbar 
or django-extensions—you’ll be familiar with the process. 
You pip install the package, then add it to your list of INSTALLED_APPS 
in settings.py—and often configure other picees, like adding something 
to MIDDLEWARE or updating your urls.py with new URL patterns.

This isn’t exactly a huge burden, but it’s added friction. It’s also the 
exact kind of thing plugin systems are designed to solve.

DJP addresses this. You configure DJP just once, and then any additional 
DJP-enabled plugins you pip install can automatically register configure 
themselves within your Django project.


Why call it DJP ? 
=====================

Because django-plugins already existed on PyPI, and I like my three 
letter acronyms there!

What’s next for DJP ? 
=========================

I presented this at DjangoCon US 2024 yesterday afternoon. 

Initial response seemed positive, and I’m going to be attending the 
conference sprints on Thursday morning to see if anyone wants to write 
their own plugin or help extend the system further.

Is this a good idea ? I think so. 

Plugins have been transformative for both Datasette and LLM, and I think 
Pluggy provides a mature, well-designed foundation for this kind of system.

I’m optimistic about plugins as a natural extension of Django’s existing 
ecosystem. Let’s see where this goes.
