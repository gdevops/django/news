

.. _david_guillot_2024_10_09:

==========================================================================
2024-10-09 **Proposal for a Django project template** by David Guillot
==========================================================================

- https://david.guillot.me/en/posts/tech/proposal-for-a-django-project-template/


🧐 Why?
=========

- Because after 7 years working on the same big Django codebase, when I 
  wanted to start a new project from scratch (a small demo for another 
  blog post that’s yet to come 😅), I had a hard time
- Because none of the Django project templates I found on the web felt 
  right to me (either too opinionated, or too weak on some aspects)
- Because tooling has evolved greatly recently

One of the reasons Django is awesome is because it’s unopinionated: it 
lets you make your own choices. 

But sometimes it’s intimidating. 

The idea here is to be opinionated on what’s around Django (Python tooling, 
structure, environment, UI tooling) but let you make your own implementation 
choices when it comes to your application.

🚤 TL;DR
================

- https://codeberg.org/David-Guillot/django-project-template


This blog post introduces a Django project template I’ve been working on 
in the past few weeks. 

You can find it here and try it by yourself (and you can also find an 
example project created using the template ), but as it’s a bit unusual,
you may want to read this.

🐍 Python dependency management: uv, what else ?
==================================================

I’ve been a long-time pip-tools + native venv (+ sometimes pyenv ) user. 

When pipenv , then poetry , then pdm came, some people tried to tell me 
that I should switch. 
I was never convinced. I was always doubtful about an all-in-one tool 
pretending to replace my custom-tailored assembly of well established 
specialized tools, I didn’t see enough value.

**But then came uv** . 

It made me realize that no matter my theoretical doubts, if a tool a 
just 100x faster, I had to embrace it.

So don’t be surprised to find this Django project template using uv.
⚙️ Task runner: just use just

I’ve been a long-time Makefile user. But I was frustrated by the .PHONY thing, 
and by the fact that some behaviors seemed weird to me (because I was 
hacking a build tool to make a task runner out of it).

just is what I needed from the beginning, and it doesn’t just avoid 
Makefile’s caveats, it also offers other advantages. 
So I’ve used it in this template.
