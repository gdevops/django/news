.. index::
   pair: ASGI ; ASGI deployment options for Django (2024-04-03, Mariusz Felisiak)
   pair: Rust ; ASGI

.. _Felisiak_2024_04_03:

=========================================================================================================
2024-04-03 **ASGI deployment options for Django** by Mariusz Felisiak  #Rust #granian #ASGI #RSGI #WSGI
=========================================================================================================

- https://fosstodon.org/@felixxm
- https://github.com/felixxm
- https://www.mariuszfelisiak.org/
- https://fly.io/django-beats/asgi-deployment-options-for-django/
- https://github.com/emmett-framework/granian
- https://github.com/emmett-framework/granian/blob/master/docs/spec/RSGI.md

.. tags:: ASGI, RSGI, HTTP, Rust, granian


Announces
===========

- https://fosstodon.org/@felixxm/112207395327155140
- https://fosstodon.org/@katianakamura/112206743824708138


Granian (A Rust HTTP server for Python applications)
=========================================================

- https://github.com/emmett-framework/granian

Granian is a **newish option in ASGI servers world**.

It is a Rust 🦀 hybrid ASGI/WSGI/RSGI server that supports HTTP/1, HTTP/2,
and WebSockets.

It can be used both during development and as a production web server.

It has a myriad of configuration options and provides an auto-reloader
(when installed with granian[reload]) that is useful for development.

Use the granian command to develop your project under ASGI:


Closing thoughts
====================

The world of ASGI servers is truly rich and everyone should find something
that suits their personal preferences.

This article mentions just some of the most popular and solid options.

Choosing an ASGI server for a production environment is like picking an ice
cream flavor - everyone has to find their favorite type based on personal taste.

Try them and share!


