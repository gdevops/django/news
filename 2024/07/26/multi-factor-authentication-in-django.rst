.. index::
   ! Multi-factor authentication

.. _das_2024_07_26:

=====================================================================
2024-07-26 **Multi-factor authentication in django** by Kushal Das
=====================================================================

- https://kushaldas.in/posts/multi-factor-authentication-in-django.html
- https://github.com/xi/django-mfa3
- https://github.com/xi/django-mfa3/pull/20 (MFA Session cookie and MFA resource evations prevention)
- https://en.wikipedia.org/wiki/Multi-factor_authentication


`Multi-factor authentication <https://en.wikipedia.org/wiki/Multi-factor_authentication>`_ 
is a must have feature in any modern web application. 

Specially providing support for both TOTP (think applications on phone) 
and FIDO2 (say Yubikeys) usage. 

I created a small Django demo mfaforgood which shows how to enable both.


I am using `django-mfa3 <https://github.com/xi/django-mfa3>`_ for all 
the hard work, but specially from a PR branch  from my friend `Giuseppe De Marco <https://github.com/peppelinux>`_.

I also fetched the `cbor-js package <https://www.npmjs.com/package/cbor-js>`_ in the repository so that hardware 
tokens for FIDO2 to work. 

**I hope this example will help you add the MFA support to your Django application**.
