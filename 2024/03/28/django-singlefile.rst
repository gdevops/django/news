.. index::
   ! Django-singlefile

.. _django_singlefile:

=============================================================================================================================================================================
2024-03-28 Django-singlefile **a small library that makes it easier to write single-file Django applications in a similar way to how you'd write Flask applications**
=============================================================================================================================================================================

- https://github.com/andrewgodwin
- https://github.com/andrewgodwin/django-singlefile


This is a small library that makes it easier to write single-file Django
applications in a similar way to how you'd write Flask applications.

It's still alpha; in particular, I'd like to:

- Add some more environment variable overrides for settings
- Add in some basic database support

But hey, it's a fun start.
