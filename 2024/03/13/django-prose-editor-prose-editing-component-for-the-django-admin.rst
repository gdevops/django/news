.. index::
   pair: Matthias; django-prose-editor – Prose-editing component for the Django admin


.. _mathias_2014_03_13:

==================================================================================================
2024-03-13 **django-prose-editor – Prose-editing component for the Django admin** by Matthias
==================================================================================================

- https://406.ch/writing/django-prose-editor-prose-editing-component-for-the-django-admin/
- https://prosemirror.net/

During the last few days I have been working on a prose-editing component
for the Django admin which will replace the basically dead django-ckeditor
in all of my projects.

It is based on `ProseMirror <https://prosemirror.net/>`_, in my opinion the greatest toolkit for building
prose editors for the web.
