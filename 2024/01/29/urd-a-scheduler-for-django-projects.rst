.. index::
   pair: urd;  A scheduler for Django projects

.. _urd_2024_01_29:

=======================================================
2024-01-29 **urd: A scheduler for Django projects**
=======================================================

- https://github.com/boxed/urd

Urd
======

Urd is a scheduler for Django projects. Some features:

- schedule < 1m time slots
- single concurrent execution [1]
- fast enable/disable [2]
- simple deployment
- no extra dependencies beyond Django
