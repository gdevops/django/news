.. index::
   ! Collapsing Django's Migrations

.. _collapsing_django_migrations:

2024-01-05 Collapsing Django's Migrations
===========================================

- https://danielquinn.org/blog/collapsing-djangos-migrations/
- https://docs.djangoproject.com/en/dev/topics/migrations/
- https://realpython.com/django-migrations-a-primer/


Long-running Django projects can start to create a lot of migrations.

After just a few years, an actively developed project can create thousands
of them!

This can put a serious dent in your test running, because (a) Django runs
the migrations at test time to setup your database, and (b) you can't test
your migrations unless you're happy to have 30min CI runs!

Migrations can also often be a painful source of technical debt, since
they sometimes import libraries that you don't use anymore, but can't
remove because someone, some day will try to run manage.py migrate
from scratch only to have it blow up looking for a dependency you don't
actually use anymore.

So, looking down the barrel of a performance, tech debt, and stability
headache, it's a good idea to pay some attention to your migrations
from time to time.
