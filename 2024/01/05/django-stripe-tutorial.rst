.. index::
   ! Django stripe tutorial

.. _django_stripe_tutorial:

===================================
2024-01-05 Django Stripe Tutorial
===================================

- https://learndjango.com/tutorials/django-stripe-tutorial


In this tutorial, we will create a Stripe-powered Django website to handle
purchases.

The final site will have a homepage for orders, a success page when an
order goes through, and a cancel page if the order fails.
