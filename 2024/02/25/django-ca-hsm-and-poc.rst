.. index::
   ! django-ca, HSM and PoC

.. _django_ca_2024_02_25:

=======================================
2024-02-25 **django-ca, HSM and PoC**
=======================================

- https://kushaldas.in/posts/django-ca-hsm-and-poc.html
- https://github.com/mathiasertl/django-ca
- https://datatracker.ietf.org/doc/html/rfc5272
- https://github.com/SUNET/python_x509_pkcs11
- https://cryptography.io/en/latest/
- https://github.com/kushaldas/django-ca/tree/pkcs11_poc


django-ca, HSM and PoC
===========================

`django-ca <https://github.com/mathiasertl/django-ca>`_ is a feature rich
**certificate authority** written in Python, using the django framework.

The project exists for long, have great documentation and code comments all around.

As I was looking around for possible CAs which can be used in multiple projects
at work, django-ca seems to be a good base fit.
Though **it has still a few missing parts (which are important for us), for example
HSM support and Certificate Management over CMS**.

I started looking into the codebase of django-ca more and meanwhile also
started cleaning up (along with Magnus Svensson) another library written at
work for HSM support.
I also started having conversion with Mathias (who is the author of django-ca)
about this feature.

Thanks to the amazing design of the Python Cryptography team, I could just add
several Private key implementations in our library, which in turn can be used
as a normal private key.

I worked on a `proof of concept branch (PoC) <https://github.com/kushaldas/django-ca/tree/pkcs11_poc>`_, while getting a lot of tests
also working::

    ===== 107 failed, 1654 passed, 32 skipped, 274 errors in 286.03s (0:04:46) =====

Meanwhile Mathias also started writing a separate feature branch where he
is moving the key operations encapsulated inside of backends, and different
backends can be implemented to deal with HSM or normal file based storage. He
then chatted with me on Signal over 2 hours explaining the code and design of
the branch he is working on. He also taught me many other django/typing things
which I never knew before in the same call. His backend based approach makes
my original intention of adding HSM support very easy. But, it also means at
first he has to modify the codebase (and the thousands of test cases) first.

I am writing this blog post also to remind folks that not every piece of code
needs to go to production (or even merged). I worked on a PoC, that validates
the idea. And then we have a better and completely different design. It is
perfectly okay to work hard for a PoC and later use a different approach.

As some friends asked on Mastodon, I will do a separate post about the cleanup
of the other library.

Mathias Ertl
=====================

- https://id.er.tl/ (Mathias Ertl: Digital identity)
- https://github.com/mathiasertl/id
- https://github.com/mathiasertl
- https://github.com/mathiasertl/django-ca/issues/129#issuecomment-1965905460

Hi @alfonsrv ,

First, a general update:

@kushaldas made significant progress on signing certificates via the PKCS11 interface
with cryptography (with more help from the cryptography maintainers - thanks!).
He has a proof-of-concept branch demonstrating it (@kushaldas , maybe you can link it?).

I myself worked a lot on building on Kushal's work and generalizing it.

See the linked branch! My current approach is as follows:

- Configuration has a CA_KEY_BACKENDS setting, very similar to Django's STORAGES or DATABASES setting.
- Users can add/remove backends as they see fit.
- Custom backends can be implemented by implementing an abstract base class.
- this interface also allows for command line integration.
- if implemented correctly, configuring a custom backend in CA_KEY_BACKENDS is
  sufficient for making django-ca use it.

About your idea: it's generally a valid idea and my approach isn't much different
in the end.
Let me explain.

The problem is that the private key is used in a variety of places. For example:

- Signing certificates (what you mentioned in profiles).
- signing certificates via API (profiles are not used there)
- signing intermediate CAs.
- signing CRLs.
- signing OCSP responder certificates.

In addition, parameters differ based on implementations (password and path for
filesystem, key slot and label for HSMs, ...), so we need command line integration.

Now that I think of it, also in the API and admin interface.

It would mean there's lots of hooks you'd have to implement and configure for a
fluent user experience.

In the end, a backend works just the same.

You still have to implement everything (but with the advantage of subclass checks),
but you only have to configure the path in a setting.

I'll invest more time in my branch this week.
I hope to get it in good condition by Sunday.
**The basic concept is proven to work, but tests are not adapted**.

In the meantime, you're of course also welcome to fork and work on your idea,
if you want.

Kr, Mat
