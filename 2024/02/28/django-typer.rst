.. index::
   pair: Django ; Typer

.. _django_typer:

====================================================================================================================
2024-02-28 **django-typer** (Use Typer (type hints) to define the interface for your Django management commands)
====================================================================================================================

- https://github.com/bckohan/django-typer
- https://django-typer.readthedocs.io/en/latest/


Use `Typer <https://typer.tiangolo.com/>`_ to define the CLI for your Django management commands.
Provides a TyperCommand class that inherits from `BaseCommand <https://docs.djangoproject.com/en/stable/howto/custom-management-commands/#django.core.management.BaseCommand>`_
and allows typer-style annotated parameter types. All of the BaseCommand functionality is
preserved, so that TyperCommand can be a drop in replacement.
