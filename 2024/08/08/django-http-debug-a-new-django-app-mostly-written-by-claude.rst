

.. _willison_2024_08_08:

==============================================================================================================
2024-08-08 **django-http-debug, a new Django app mostly written by Claude** by Simon Willison
==============================================================================================================

- https://simonwillison.net/2024/Aug/8/django-http-debug/
- https://github.com/simonw/django-http-debug
- :ref:`pv_links:carlini_2024_08_01`

Developer: Simon Willison
============================

- :ref:`python:simon_willison`

Introduction
==============

Yesterday I finally developed something I’ve been casually thinking
about building for a long time: django-http-debug. It’s a reusable Django
app—something you can pip install into any Django project—which provides
tools for quickly setting up a URL that returns a canned HTTP response and
logs the full details of any incoming request to a database table.


Was Claude worth it ? 
=======================

This entire project took about two hours—just within a tolerable amount of
time for what was effectively a useful sidequest from my intended activity
for the day.

Claude didn’t implement the whole project for me. The code it produced
didn’t quite work—I had to tweak just a few lines of code, but knowing
which code to tweak took a development environment and manual testing and
benefited greatly from my 20+ years of Django experience!

This is yet another example of how LLMs don’t replace human developers:
they augment us.

The end result is a tool that I’m already using to solve real-world
problems, and a code repository that I’m proud to put my name to. Without
LLM assistance this project would have stayed on my ever-growing list of
“things I’d love to build one day”.

I’m also really happy to have my own documented solution to the challenge
of adding automated tests to a standalone reusable Django application. I was
tempted to skip this step entirely, but thanks to Claude’s assistance I
was able to break that problem open and come up with a solution that I’m
really happy with.

