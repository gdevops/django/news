

.. _gibson_2024_08_31:

==================================================================
2024-08-31 **Evolving Django’s auth.User** by Carlton Gibson
==================================================================

- https://buttondown.com/carlton/archive/evolving-djangos-authuser

Announce
===========

- https://fosstodon.org/@carlton/113056255748047320

Gah! That took longer than I thought. I'm exhausted. 😅

This month's Stack Report is going out now. 
A free-access edition giving my far-too-long thoughts on how we can Evolve Django's auth.User. 

- https://buttondown.com/carlton/archive/evolving-djangos-authuser/ #Django #TheStackReport


Evolving Django’s auth.User
=================================

Folks will occasionally ask if I've got an opinion on some topic Django 
related. 
I'll reply that I've got an opinion on everything, so I've certainly got 
an opinion on that.

In Django terms most of the opinions I hold are quite conservative. 
Django has a grain. It wants you to do things a certain way. That way is 
proven by the test of time, and my view is, roughly, that the more you 
lean into it, the happier, and more successful, you are going to be.

“Conservative” here doesn't mean standing still. It is exciting times for 
Django. The last several years have added a whole host of a new features 
and improvements, and I see plenty of room to keep pushing the framework forward.

My recent contributions, in the form of Neapolitan and django-template-partials, 
say, aren't radical – rather they’re very much more of the same – they're more Django – 
so whilst new, they're quite orthodox too.


The Complexity Tax
====================================

Django is hard to get started with. Custom user models make it harder.

It’s not easy for anyone to get going with their first web framework. 
There’s a learning curve, and some of that is essential to the subject. 
We can make the point time and time again that under the hood Django is 
doing exactly the same as every other framework out there, in almost 
exactly the same way but, we’re adding accidental complexity to that 
where other web frameworks don’t. 

We’re not setting up Django well for beginners.

This is typical: Django is considered "difficult to learn". 
Go read that piece. Same link: Our headline introductory tutorial is known 
as the `"infamous polls tutorial" <https://testdriven.io/blog/django-vs-flask/>`_


Related Links : Login via email instead of username
======================================================

- https://falco.oluwatobi.dev/the_cli/start_project/packages.html#login-via-email-instead-of-username


The email field is configured as the login field using django-allauth. 

The username field is still present but is not required for login. 
Allauth automatically fills it with the part of the email before the @ symbol. 

More often then not when I create a new django project I need to use 
something other than the username field provided by django as the unique 
identifier of the user, and the username field just becomes an annoyance 
to deal with. 

It is also more common nowadays for modern web and mobile applications 
to rely on a unique identifier such as an email address or phone number 
instead of a username.
