.. index::
   pair: login ; django-allauth
   pair: Will Vincent ; Django Log In with Email not Username (2024-08-23)

.. _vincent_2024_08_23:

========================================================================
2024-08-23 **Django Log In with Email not Username** by Will Vincent
========================================================================

- https://learndjango.com/tutorials/django-log-in-email-not-username
- https://github.com/pennersr/django-allauth
- https://wsvincent.com/about/
- :ref:`william_vincent`


Introduction
=================

- https://docs.djangoproject.com/en/dev/ref/contrib/auth/#user-model

Django was first released in 2005, and since then, a lot has changed in 
web development. 

Notably, the predominant pattern of using username/email/password has been 
simplified to just email/password. However, due to legacy reasons around 
the built-in `Django User model https://docs.djangoproject.com/en/dev/ref/contrib/auth/#user-model <https://docs.djangoproject.com/en/dev/ref/contrib/auth/#user-model>`_, 
it can take a few extra steps to implement this change in practice.

In this tutorial, we'll walk through how to implement email and password 
only for sign-up and log-in on a new Django project using a custom user model 
and the popular django-allauth package.

