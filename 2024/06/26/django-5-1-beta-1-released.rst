
.. index::
   pair: Django 5.1b1 ; 2024-06-26 

.. _django_5_1_beta_1_2024_06_26:

======================================================
2024-06-26 **Django 5.1 beta 1 is now available**
======================================================

- https://www.djangoproject.com/weblog/2024/jun/26/django-51-beta-1-released/


Posted by Natalia Bidart on juin 26, 2024

Django 5.1 beta 1 is now available
===================================

It represents the second stage in the 5.1 release cycle and is an 
opportunity for you to try out the changes coming in Django 5.1.

Django 5.1 brings a kaleidoscope of improvements which you can read about 
in the in-development 5.1 release notes.

Only bugs in new features and regressions from earlier versions of Django 
will be fixed between now and the 5.1 final release. 

Translations will be updated following the "string freeze", which occurs 
when the release candidate is issued. 

The current release schedule calls for a release candidate in a month 
from now, and a final release to follow about two weeks after that, 
scheduled for August 7th.

Early and frequent testing from the community will help minimize the 
number of bugs in the release. Updates on the release schedule are 
available on the Django forum.

As with all alpha and beta packages, this is not for production use. 

But if you'd like to take some of the new features for a spin, or to help 
find and fix bugs (which should be reported to the issue tracker), you 
can grab a copy of the beta package from our downloads page or on PyPI.

The PGP key ID used for this release is Natalia Bidart: 2EE82A8D9470983E.
