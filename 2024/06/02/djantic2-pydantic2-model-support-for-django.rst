

.. _djantic2_2024_06_02:

======================================================================================================================================================================================================================
2024-06-02 Djantic2 is a fork of djantic which works with pydantic >2, it is a library that provides a configurable utility class for automatically creating a Pydantic model instance for any Django model class
======================================================================================================================================================================================================================

- https://mastodon.social/@python_discussions/112548645098536466
- https://github.com/jonathan-s/djantic2
- https://docs.pydantic.dev/latest/concepts/models/

Djantic2 is a fork of djantic which works with pydantic >2, it is a library
that provides a configurable utility class for automatically creating a
Pydantic model instance for any Django model class.

It is intended to support all of the underlying Pydantic model functionality
such as JSON schema generation and introduces custom behaviour for exporting
Django model instance data.


.. code-block:: python

    from users.models import User
    from pydantic import ConfigDict

    from djantic import ModelSchema

    class UserSchema(ModelSchema):
        model_config = ConfigDict(model=User, include=["id", "first_name"])

    print(UserSchema.schema())


output::

    {
        "description": "A user of the application.",
        "properties": {
            "id": {
                "anyOf": [{"type": "integer"}, {"type": "null"}],
                "default": None,
                "description": "id",
                "title": "Id",
            },
            "first_name": {
                "description": "first_name",
                "maxLength": 50,
                "title": "First Name",
                "type": "string",
            },
        },
        "required": ["first_name"],
        "title": "UserSchema",
        "type": "object",
    }


.. code-block:: python

    user = User.objects.create(
        first_name="Jordan",
        last_name="Eremieff",
        email="jordan@eremieff.com"
    )

    user_schema = UserSchema.from_orm(user)
    print(user_schema.json(indent=2))
