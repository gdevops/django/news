#!/bin/bash
# ci_delete_pipelines.bash
# set the Personal Access Token GITLAB_TOKEN before;
# use a .envrc file with the direnv command (https://asdf-vm.com/guide/introduction.html#direnv)
set -e
for id in `glab pipeline list --sort asc -P 3| awk '{print substr($3,2)}'| tail -n+2`
do
    glab pipeline delete $id
done
