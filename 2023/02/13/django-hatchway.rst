


.. _django_hatchway_ref:

=============================================================================================================================================================
**django-hatchway** (an API framework inspired by the likes of FastAPI, but while trying to keep API views as much like standard Django views as possible)
=============================================================================================================================================================

- :ref:`django_hatchway`
