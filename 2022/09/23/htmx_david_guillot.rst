

.. _htmx_david_guillot_2022_09_23:

=====================================================================================================
**From React to htmx on a real-world SaaS product: we did it, and it's awesome!** by David Guillot
=====================================================================================================

- https://2022.djangocon.eu/


David Guillot
===============

- https://discord.com/channels/725789699527933952/864934037381971988/997611808129024050
- https://pretalx.evolutio.pt/djangocon-europe-2022/talk/MZWJEA/
- https://pretalx.evolutio.pt/djangocon-europe-2022/speaker/GA3TMV/
- https://www.contexte.com/

.. figure:: images/david_guillot.png
   :align: center


I have been a developer for 15 years.
First bare PHP, then PHP/Symfony2, then a bit of Java/SprintBoot, and
now happy with Django for the past 5 years.

I care mainly about building good products with a team I'm proud of being
part of.

I'm also interested on how we can, as software developers, reduce the
environmental footprint of what we create.


Description
==============

We took the plunge and replaced the 2-year-of-work React UI of our SaaS
product with simple Django templates and htmx in a couple of months.

We’d like to share our experience with you, with concrete indicators on
various aspects, and convince your CTO!

🔍 You’ve probably heard of `htmx <https://htmx.org/>`_, maybe seen talks about it (maybe even
right `here last year <https://www.youtube.com/watch?v=Zs0DXR1S03M>`_).

**Demos are great, potential seems enormous.**

Maybe you've heard it's great for quick prototyping and tried it.

❓ But what about switching for your real-life project, against everything
you’ve heard since 2016 (”modern web interfaces need Javascript-driven webapps”,
"the best SaaS products are made of an SPA", etc.)?

Does htmx keep its promises? What impact on your product, your team, your business?

✅ That's what we just did at `Contexte <https://www.contexte.com/>`_, by **getting rid of the React SPA
of a SaaS product**.

It wasn't a easy decision to make (because the UI holds some fairly complex
interactions and we were told such interactions require client-side state
management), but it's now such a relief.

Maybe if we share our experience the decision would be easier to make
for others.

💬 Draft outline of the talk:

- What is Contexte? mainly about our Product/Tech team size and organization
- What is our first SaaS product, Contexte Scan (click here for a
  Google-translated product presentation)? a quick demo, with an emphasis
  on the most complex UI elements (I'm not here to sell it to you 😉 )
- Our React UI: bulky, buggy, constantly refactored, dependency hell
  (quick tour, some figures)
- Our htmx UI: a bit of code here, with examples of how you replace
  client-side state management with just some htmx-loaded HTML fragments
- Impact on end-users: same UX, better performances
- Impact on the team: faster, fullstack, more agile
- Impact on the planet: smaller footprint on end-users computers
- Conclusion: go for it!


Adam Johnson
==============

- https://x.com/AdamChainz/status/1573088894408863744?s=20&t=fIXndFqiSLbgRpJAundX5A


This talk was brilliant, it showed real world outcomes of moving from
a JS rendered site to plain HTML

Carson Gross
=============

- https://x.com/htmx_org/status/1573156869367021571


A real-world move to htmx from react:

- reduced the code base size by 67%
- reduced the total JS dependencies by 96%
- reduced the build time by 88%

not saying everyone will see these numbers, or that htmx is right for
everything, but...

that's pretty sweet
