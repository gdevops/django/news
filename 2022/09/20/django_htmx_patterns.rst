
.. _django_htmx_patterns:

===============================================================================================
Sharing patterns I use with Django and htmx by Luke Plant (https://x.com/spookylukey)
===============================================================================================

- https://github.com/spookylukey/django-htmx-patterns
- https://x.com/pycoders/status/1569326718824890374?s=20&t=7o4LQKfSo-Ou3uNlsiKxnw
- https://django-htmx-patterns.spookylukey1.repl.co/


Demo
=====

- https://django-htmx-patterns.spookylukey1.repl.co/

Django + htmx patterns
======================

This repo is a rough-and-ready compilation of the patterns I’m using and
developing when writing `Django <https://www.djangoproject.com/>`_ projects that
use `htmx <https://htmx.org/>`_.

The docs are intended to be viewed on GitHub:
https://github.com/spookylukey/django-htmx-patterns/ and the code can be run
locally or on repl.it (see below)

My aims are to document and share:

1. The basic patterns and control flow needed
2. Some enhancements we can make with a few utilities

The code presented depends only on Django and htmx, unless otherwise noted. I
wont be packaging it up as a library. It is instead being released into the
public domain, and you are encouraged to copy-paste it for your own needs. (See
the “Approach” docs for why).

There are some Django packages that provide shortcuts for some of the things
mentioned here, such as `django-htmx
<https://github.com/adamchainz/django-htmx>`_, but I won’t be using any of them
for the sake of clarity.
