

.. _django_shark_2022_05_09:

=========================================================================
2022-05-09 10 reasons MVC frameworks aren't dinosaurs but sharks
=========================================================================

- https://www.david-dahan.com/blog/10-reasons-mvc-frameworks-arent-dinosaurs-but-sharks
- https://www.simplethread.com/relational-databases-arent-dinosaurs-theyre-sharks/


Introduction
============

.. figure:: images/htmx_shark.png
   :align: center


3 - The revolution HTML Over The Wire is coming
==================================================

Last years, using MVC framework was not an option as soon as you wanted
a dynamic front-end. Having to reload a full page for every user action
leads to a bad user experience and is indeed, not acceptable in 2022.

To add some dynamism to your page, you had to mess with jQuery and AJAX,
which often lead to very complex and hacky code in y opinion. That's why
many of us started using SPAs when AngularJS came out.
It was a true revolution!

Hotwire is the dedicated solution for Rails, made by its creator.

HTMX is a more generic solution usable with any HTML. I suspect lots of
SPAs built today could be replaced by this approach, with more benefits
that losses.

Folrimond
===========

- :ref:`tuto_htmx:florimond_2022_05_08`
