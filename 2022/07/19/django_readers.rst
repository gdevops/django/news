

.. _django_readers_2022_07_19:

===========================================================================
2022-07-19 Exciting news: django-readers now has proper documentation!
===========================================================================

- :ref:`django_readers`
- https://www.django-readers.org/
- https://github.com/dabapps/django-readers/tree/main/docs


Twitter Announce
=================

- https://x.com/j4mie/status/1549372145918267394?s=20&t=Ekc7ENzENJztmME9i6iSXA

Exciting news: django-readers now has proper documentation! https://django-readers.org 🎉

.. figure:: on_twitter.png
   :align: center
