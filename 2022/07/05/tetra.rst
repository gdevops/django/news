.. index::
   pair: Tetra; Django

.. _tetra_2022_07_05:

============================================================================================
2022-07-05  **Tetra - A full stack component framework for Django using Alpine.js**
============================================================================================

- https://github.com/samwillis/tetra
- https://x.com/samwillis
- https://discord.com/channels/725789699527933952/725789747212976259/993556600562405526
- https://www.pythonpodcast.com/tetra-full-stack-component-web-framework-episode-369/


Sam Willis
============

- https://x.com/samwillis

I make stuff. Multidisciplinary Developer, Designer and Engineer.
Building http://tetraframework.com.

- Creator of http://playsudokutoo.com.
- Co-founder of @posterhaste
- Stamford UKsamwillis.co.uk Joined May 2007

Announce (2022-05-24)
=========================

- https://x.com/samwillis/status/1529089617894510592?s=20&t=UmpzoVo0cHtbmntL6CkGkQ

::

    Super excited to be releasing the first version of Tetra, a full stack
    component framework for Django using Alpine.js

    https://tetraframework.com
    It lets you build "hybrid" components - combining server state and
    user interactions seamlessly.

    Take a look!

- https://x.com/carltongibson/status/1529363195021996032?s=20&t=UmpzoVo0cHtbmntL6CkGkQ

::

    This looks really cool @samwillis! You should submit a talk for it to
    @DjangoConEurope


What does Tetra do ?
=====================

Django on the backend, Alpine.js in the browser
-------------------------------------------------

Tetra combines the power of Django with Alpine.js to make development
easier and quicker.

Component encapsulation
--------------------------

Each component combines its Python, HTML, CSS and JavaScript in one place
for close proximity of related code.

Resumable server state
----------------------------

The components' full server state is saved between public method calls.
This state is encrypted for security.

Public server methods
----------------------------

Methods can be made public, allowing you to easily call them from JS on
the front end, resuming the component's state.

Shared public state
-----------------------

Attributes can be decorated to indicate they should be available in the
browser as Alpine.js data objects.

Server "watcher" methods
---------------------------

Public methods can be instructed to watch a public attribute, enabling
reactive re-rendering on the server.

Inplace updating from the server
--------------------------------------

Server methods can update the rendered component in place.
Powered by the Alpine.js morph plugin.

Component library packaging
-------------------------------

Every component belongs to a "library"; their JS & CSS is packed together
for quicker browser downloads.

Components with overridable blocks
--------------------------------------

Component can have multiple {% block(s) %} which can be overridden when used.

JS/CSS builds using [esbuild](https://esbuild.github.io)
-----------------------------------------------------------

Both for development (built into runserver) and production your JS & CSS
is built with esbuild.

Source Maps
----------------

Source maps are generated during development so that you can track
down errors to the original Python files.

Syntax highlighting with type annotations
-------------------------------------------

Tetra uses type annotations to syntax highlight your JS, CSS & HTML in
your Python files with a [VS Code plugin](https://github.com/samwillis/python-inline-source/tree/main/vscode-python-inline-source)


https://news.ycombinator.com/item?id=31526684
================================================

So in that stack Tetra would replace htmx (which is an awesome tool!)
and takes control of your Alpine components.

Tetra provides the "glue" between Django and Alpine, but also encapsulates
your components so all html/css/python/js thats related is next to each
other.

You wouldn't need to be writing views that return html fragments for htmx.

One of its big features though is that your server rendered views can be
reactive, re-rendering on the server and updating in the browser based
on user interactions a client side state changes.

You have to write less glue code to make everything work with Tetra.


Tetra on the other hand is very "component" focused, everything is built
up out of nested components. These are rendered on the server and a
diff/merge is performed by the Alpine.js Morph plugin to update the dom.

So Tetra doesn't re-render the whole page, only the component that changes,
therefore it probably sits somewhere between Unpoly and HTMX, but I have
done no benchmarking.

Tetra is also tightly linked to Python, each components server state
is "frozen" and sent to the browser when it first renders (encrypted and signed).

If you then call any public methods on the component the state is sent
back to the server and "resumed".
This makes the component fully statefull between server public method
calls.
Neither HTMX or Unpoly have this, you have to manually implement the
end points for all updates. Tetra does that all for you.
