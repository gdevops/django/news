.. index::
   pair: nodeenv; Javascript

.. _nodeenv:

============================================================================================
2022-07-05  **nodeenv and nodejs-bin**
============================================================================================



Remark from @pradyunsg
=========================

- https://x.com/pradyunsg/status/1540237848741351426?s=20&t=OBayyWnmBFgXcVUr7TT3bg


::

    I’m seeing a bunch of excitement around NodeJS/npm being packaged
    into a Python package with an API.
    I guess it’s a good time to chime in that nodeenv exists.


    If you're using pre-commit, you're using this. It's existed for
    many years and works as advertised.


nodeenv
========

- https://github.com/ekalinin
- https://github.com/ekalinin/nodeenv
- http://ekalinin.github.io/nodeenv/


Overview
-----------

**nodeenv** (node.js virtual environment) is a tool to create isolated
node.js environments.

It creates an environment that has its own installation directories, that
doesn't share libraries with other node.js virtual environments.

Also the new environment can be integrated with the environment which
was built by virtualenv (python).

If you use nodeenv feel free to add your project on wiki: Who-Uses-Nodeenv.

Recalling by
---------------

- https://x.com/pradyunsg/status/1540237848741351426?s=20&t=s0sNoI6N36rUKnrmt9LWzw

::

    I’m seeing a bunch of excitement around NodeJS/npm being packaged
    into a Python package with an API.
    I guess it’s a good time to chime in that nodeenv exists.


nodejs-bin
================

- https://github.com/samwillis
- https://x.com/samwillis
- https://github.com/samwillis/nodejs-pypi
