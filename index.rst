
.. index::
   pair: Django ; news

.. raw:: html

   <a rel="me" href="https://framapiaf.org/@pvergain"></a>
   <a rel="me" href="https://babka.social/@pvergain"></a>


|FluxWeb| `RSS <https://gdevops.frama.io/django/news/rss.xml>`_

.. _django_news:

=================
**Django News**
=================

- https://django-news.com
- https://django-news.com/issues


.. toctree::
   :maxdepth: 5

   2025/2025
   2024/2024
   2023/2023
   2022/2022
   2021/2021

.. toctree::
   :maxdepth: 3

   2020/2020
   2019/2019
   2016/2016
